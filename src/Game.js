class Game extends Phaser.Scene {
    constructor() {
        super({key: 'Game'});

        this.BRICK = 'brick';
        this.BUILDER = 'builder';
        this.MAX_BRICKS = 2;
        
        this.bricks = [];
        this.tick = 0;
    }

    preload() {
        this.load.image(this.BRICK, 'assets/brick.png');
        this.load.spritesheet(this.BUILDER, 'assets/builder.png',
            {frameWidth: 48, frameHeight: 40});
    }

    create() {
        this.anims.create({
            key: 'right',
            frames: this.anims.generateFrameNumbers(this.BUILDER, {start: 0, end: 1}),
            frameRate: 8,
            repeat: -1
        });
        this.anims.create({
            key: 'left',
            frames: this.anims.generateFrameNumbers(this.BUILDER, {start: 2, end: 3}),
            frameRate: 8,
            repeat: -1
        });

        this.createBuilder();

        this.input.on('pointerdown', (event) => {
            if (this.bricks.length < this.MAX_BRICKS) {
                const brick = this.physics.add.image(event.x, 0, this.BRICK);
                this.bricks.push(brick);

                this.physics.add.collider(this.builder, brick, 
                    (builder, brick) => this.respawnBuilder());
            }
        }, this);

    }
    
    update(time, delta) {
        this.tick += delta;
        if (this.tick > 1000) {
            if(Phaser.Math.Between(0, 10) > 5) {
                this.builder.setVelocityX(-this.builder.body.velocity.x);
            }
            this.tick = 0;
        }

        if (this.builder.body.velocity.x < 0) {
            this.builder.play('left', true);
        }
        else {
            this.builder.play('right', true);
        }

        let toRemove = this.bricks.filter((brick) => (brick.body.y > 710));
        this.bricks = this.bricks.filter((brick) => (brick.body.y < 710));
        for (let brick of toRemove) {
            brick.destroy();
        }

    }

    createBuilder() {
        this.builder = this.physics.add.sprite(
            Phaser.Math.Between(50, 450), 
            Phaser.Math.Between(500, 650), this.BUILDER);
        this.builder.setBounce(1.0);
        this.builder.setCollideWorldBounds(true);
        this.builder.setVelocityX(Phaser.Math.Between(150, 300));
    }

    respawnBuilder() {
        this.builder.destroy();
        for(const brick of this.bricks) {
            brick.destroy();
        }
        this.bricks = [];
        this.createBuilder();
    }
}

export { Game };
