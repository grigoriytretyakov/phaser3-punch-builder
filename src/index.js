import 'phaser';

import { Game } from './Game.js';


const config = {
    type: Phaser.AUTO,

    parent: 'game',
    width: 500,
    height: 700,

    backgroundColor: 0x453748,

    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 400 },
            debug: false
        }
    },

    scene: [
        Game,
    ]
}

new Phaser.Game(config);
