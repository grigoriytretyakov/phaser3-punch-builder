webpackJsonp([0],{

/***/ 434:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

__webpack_require__(210);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GameScene = function (_Phaser$Scene) {
    _inherits(GameScene, _Phaser$Scene);

    function GameScene() {
        _classCallCheck(this, GameScene);

        var _this = _possibleConstructorReturn(this, (GameScene.__proto__ || Object.getPrototypeOf(GameScene)).call(this, { key: 'GameScene' }));

        _this.BRICK = 'brick';
        _this.BUILDER = 'builder';
        _this.MAX_BRICKS = 2;

        _this.bricks = [];
        return _this;
    }

    _createClass(GameScene, [{
        key: 'preload',
        value: function preload() {
            this.load.image(this.BRICK, 'assets/brick.png');
            this.load.spritesheet(this.BUILDER, 'assets/builder.png', { frameWidth: 48, frameHeight: 40 });
        }
    }, {
        key: 'create',
        value: function create() {
            var _this2 = this;

            this.anims.create({
                key: 'right',
                frames: this.anims.generateFrameNumbers(this.BUILDER, { start: 0, end: 1 }),
                frameRate: 3,
                repeat: -1
            });
            this.anims.create({
                key: 'left',
                frames: this.anims.generateFrameNumbers(this.BUILDER, { start: 2, end: 3 }),
                frameRate: 3,
                repeat: -1
            });

            this.createBuilder();

            this.input.on('pointerdown', function (event) {
                if (_this2.bricks.length < _this2.MAX_BRICKS) {
                    var brick = _this2.physics.add.image(event.x, 0, _this2.BRICK);
                    _this2.bricks.push(brick);

                    _this2.physics.add.collider(_this2.builder, brick, function (builder, brick) {
                        return _this2.respawnBuilder();
                    });
                }
            }, this);
        }
    }, {
        key: 'update',
        value: function update(time, delta) {
            if (this.builder.body.velocity.x < 0) {
                this.builder.play('left', true);
            } else {
                this.builder.play('right', true);
            }

            var toRemove = this.bricks.filter(function (brick) {
                return brick.body.y > 710;
            });
            this.bricks = this.bricks.filter(function (brick) {
                return brick.body.y < 710;
            });
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = toRemove[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var brick = _step.value;

                    brick.destroy();
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }, {
        key: 'createBuilder',
        value: function createBuilder() {
            this.builder = this.physics.add.sprite(Phaser.Math.Between(50, 750), Phaser.Math.Between(500, 650), this.BUILDER);
            this.builder.setBounce(1.0);
            this.builder.setCollideWorldBounds(true);
            this.builder.setVelocityX(Phaser.Math.Between(200, 400));
        }
    }, {
        key: 'respawnBuilder',
        value: function respawnBuilder() {
            this.builder.destroy();
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = this.bricks[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var brick = _step2.value;

                    brick.destroy();
                }
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            this.bricks = [];
            this.createBuilder();
        }
    }]);

    return GameScene;
}(Phaser.Scene);

var config = {
    type: Phaser.AUTO,

    parent: 'game',
    width: 800,
    height: 700,

    backgroundColor: 0x483737,

    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 400 },
            debug: false
        }
    },

    scene: [GameScene]
};

new Phaser.Game(config);

/***/ })

},[434]);